<h2 align="center">VALIDATEUR D'ADRESSE IP</h2>
<p align="center"><a href="#" style="pointer-events: none;"><img src="https://img.shields.io/badge/java-ea2d2e?&style=for-the-badge" alt="JAVA" title="JAVA" height="25"></a></p>

<p>
Rédiger le code et les tests d'un validateur d'adresse IP incluant les critères suivants.
L'adresse IP:<br/>
- doit comprendre 4 blocs<br/>
- doit être composée de nombres entiers de 0 à 255<br/>
- ne doit pas de contenir de caractères spéciaux<br/>
- ne doit pas contenir de symboles<br/>
- ne doit pas contenir de valeurs négatives<br/>
</p>

<h3>CONTRIBUTORS</h3>
If you would like to contribute to this repository, feel free to send a pull request, and I will review your code. Also feel free to post about any problems that may arise in the issues section of the repository.