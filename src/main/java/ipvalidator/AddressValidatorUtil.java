package ipvalidator;

public final class AddressValidatorUtil {
	// découpe la chaine de caractères à chaque point
    private static final String SPLIT_POINT = "\\.";

    private AddressValidatorUtil() {
        // nothing for now
    }

    public static boolean isValidAddress(String address) {
        // si l'adresse se termine par un point
        if (address.endsWith(".")){
            return false;
        }
        // si l'adresse commence par un point
        if (address.startsWith(".")){
            return false;
        }
        // slip est une méthode qui découpe la chaine de carcatères
        // recupère les blocs dans un tableau
        String[] spliteAddress = address.split(SPLIT_POINT);
        // si la longueur du tableau est égale à 4
        if (spliteAddress.length == 4) {
            // boucle sur chaque bloc
            for (String str : spliteAddress){
                try {
                    // on récupère la valeur
                    // la méthode parseInt convertie une chaine de caractère en entier
                    int value = Integer.parseInt(str);
                    // vérifie si la valeur n'est pas strictement supérieure à 255 ou strictement inférieure à 0
                    if (value > 255 || value < 0) {
                        return false;
                    }
                // parseInt peut lever une exception de type NumberFormatException
                // si la chaine de caractère passée en paramètre ne contient pas un entier convertible, retourne un false, pas une erreur
                } catch (NumberFormatException e){
                    return false;
                }
            }
        } else {
        // sinon l'adresse n'est pas valide
        return false;
        }
        // l'adresse est valide
        return true;
    }
}
